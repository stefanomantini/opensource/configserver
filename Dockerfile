FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/*.jar /app.jar
RUN echo $BITBUCKET_BUILD_NUMBER > /$BITBUCKET_BUILD_NUMBER.version
VOLUME config
ENV CONFIG_SERVER_PORT 9999
ENV CONFIG_SERVER_GIT_URI /config
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]